﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MvcCompanyEF.Models
{
    public class Employee
    {
        [Key]
        public int EmpID { get; set; }

        [StringLength(50, ErrorMessage = "FirstName cannot be longer than 50 characters.")]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Display(Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [StringLength(50)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        [Display(Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        //Display only the Date portion from DateTime
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date of Hire")]
        public DateTime HireDate { get; set; }

        public string Status { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName ="money")] //"money" required to use SQL money type instead of decimal
        public decimal Salary { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage ="Invalid Email Address")]
        public string Email { get; set; }
    }
}