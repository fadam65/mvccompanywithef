﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace MvcCompanyEF.Models
{
    public class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)] //Lets me create my own primary key
        public int DeptID { set; get; }

        [Required]
        [Display(Name = "Department Name")]
        public string DepartmentName { get; set; }

        public virtual ICollection<Recruitment> Recruitments { get; set; }
    }
}