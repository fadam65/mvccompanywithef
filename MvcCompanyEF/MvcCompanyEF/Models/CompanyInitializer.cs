﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MvcCompanyEF.Models
{
    public class CompanyInitializer : DropCreateDatabaseAlways<MvcCompanyContext>
    {
        protected override void Seed(MvcCompanyContext context)
        {
            var employees = new List<Employee>
            {
                new Employee{FirstName="Frank", LastName="Adames", HireDate=DateTime.Parse("2015-01-05"),
                    Status = "Active", Salary=67000, Email="fadames@mvc.com"},
                new Employee { FirstName = "Yan", LastName = "Li", HireDate = DateTime.Parse("2016-08-17"),
                    Status = "Active", Salary = 82000, Email = "yli@mvc.com" },
                new Employee { FirstName = "Edward", LastName = "Short", HireDate = DateTime.Parse("2012-01-01"),
                    Status = "Active", Salary = 56000, Email = "eshort@mvc.com" },
                new Employee { FirstName = "Daniel", LastName = "Strauss", HireDate = DateTime.Parse("2016-6-26"),
                    Status = "Active", Salary = 62000, Email = "dstrauss@mvc.com" },
                new Employee { FirstName = "Denise", LastName = "Jeffries", HireDate = DateTime.Parse("2010-02-07"),
                    Status = "Active", Salary = 60000, Email = "djeffries@mvc.com" },
                new Employee { FirstName = "Lilia", LastName = "Consuela", HireDate = DateTime.Parse("2011-12-01"),
                    Status = "Active", Salary = 69000, Email = "lconsuela@mvc.com" },
                new Employee { FirstName = "Anthony", LastName = "Camillo", HireDate = DateTime.Parse("2014-05-11"),
                    Status = "Active", Salary = 85000, Email = "acamillo@mvc.com" },
                new Employee { FirstName = "Carol", LastName = "Althouse", HireDate = DateTime.Parse("2012-03-01"),
                    Status = "Active", Salary = 54000, Email = "calthouse@mvc.com" },
                new Employee { FirstName = "Nestor", LastName = "Samson", HireDate = DateTime.Parse("2015-08-09"),
                    Status = "Active", Salary = 73000, Email = "nsamson@mvc.com" },
                new Employee { FirstName = "Keith", LastName = "Wright", HireDate = DateTime.Parse("2012-11-12"),
                    Status = "Active", Salary = 92000, Email = "kwright@mvc.com" },
            };
            employees.ForEach(e => context.Employees.Add(e));
            context.SaveChanges();

            var departments = new List<Department>
            {
                new Department{DeptID=1050, DepartmentName="Finance"},
                new Department{DeptID=4022, DepartmentName="Human Resources"},
                new Department{DeptID=1045, DepartmentName="Information Technology"},
                new Department{DeptID=3141, DepartmentName="Marketing"},
                new Department{DeptID=2042, DepartmentName="Services"},
            };
            departments.ForEach(d => context.Departments.Add(d));
            context.SaveChanges();

            var recruitments = new List<Recruitment>
            {
                new Recruitment{ EmployeeID=1, DepartmentID=1045 },
                new Recruitment{ EmployeeID=2, DepartmentID=1045 },
                new Recruitment{ EmployeeID=3, DepartmentID=4022 },
                new Recruitment{ EmployeeID=4, DepartmentID=3141 },
                new Recruitment{ EmployeeID=5, DepartmentID=1050 },
                new Recruitment{ EmployeeID=6, DepartmentID=1050 },
                new Recruitment{ EmployeeID=7, DepartmentID=3141 },
                new Recruitment{ EmployeeID=8, DepartmentID=3141 },
                new Recruitment{ EmployeeID=9, DepartmentID=2042 },
                new Recruitment{ EmployeeID=10, DepartmentID=2042 },
            };
            recruitments.ForEach(r => context.Recruitments.Add(r));
            context.SaveChanges();

            base.Seed(context);
        }
    }
}