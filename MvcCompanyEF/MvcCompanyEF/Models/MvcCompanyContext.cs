namespace MvcCompanyEF.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MvcCompanyContext : DbContext
    {
        // Your context has been configured to use a 'MvcCompanyContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'MvcCompanyEF.Models.MvcCompanyContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'MvcCompanyContext' 
        // connection string in the application configuration file.
        public MvcCompanyContext()
            : base("name=MvcCompanyContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Recruitment> Recruitments { get; set; }
    }
}