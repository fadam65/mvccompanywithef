﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcCompanyEF.Models;
using System.Threading.Tasks;

namespace MvcCompanyEF.Controllers
{
    public class EmployeesController : Controller
    {
        private MvcCompanyContext db = new MvcCompanyContext();

        // GET: Employees
        public ViewResult Index(string SortOrder, string SearchString)
        {
            //Add sorting to List of Employees
            ViewBag.NameSortParm = String.IsNullOrEmpty(SortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm= SortOrder == "Date" ? "date_desc" : "Date";
            var employees = from e in db.Employees select e;

            if (!String.IsNullOrEmpty(SearchString))
            {
                employees = employees.Where(e => e.LastName.Contains(SearchString)
                                            || e.FirstName.Contains(SearchString));
            }

            switch(SortOrder)
            {
                case "name_desc":
                    employees = employees.OrderByDescending(e => e.LastName);
                    break;
                case "Date":
                    employees = employees.OrderBy(e => e.HireDate);
                    break;
                case "date_desc":
                    employees = employees.OrderByDescending(e => e.HireDate);
                    break;
                default:
                    employees = employees.OrderBy(e => e.LastName);
                    break;
            }
            return View(employees.ToList());
        }

        // GET: Employees/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // GET: Employees/Create
        public ActionResult Create()
        {

            return View();
        }

        // POST: Employees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "FirstName,LastName,HireDate,Status,Salary")] Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Create Email Address and assign, if input is valid
                    string EmailAddress = employee.FirstName[0] + employee.LastName + "@mvc.com";
                    employee.Email = EmailAddress;

                    db.Employees.Add(employee);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            } catch (DataException e)
            {
                ModelState.AddModelError(e.Message, "Unable to save the Employee in the database. Try again.");
            }
            return View(employee);
        }

        // GET: Employees/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var EmployeeToUpdate = db.Employees.Find(id);
            if(TryUpdateModel(EmployeeToUpdate, "", 
                new string[] { "FirstName", "LastName", "HireDate", "Status", "Salary"}))
            {
                try
                {
                    db.SaveChanges();
                    return RedirectToAction("Index");
                } catch (DataException e)
                {
                    ModelState.AddModelError(e.Message, "Unable to save Employee information. Try again.");
                }
            }
            return View(EmployeeToUpdate);
        }

        // GET: Employees/Delete/5
        public ActionResult Delete(int? id, bool? saveChangesError=false)
        {
            if (id == null) 
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Redirect an error message if SaveChanges() fail during POST
            if (saveChangesError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "Error: Delete failed. Try again.";
            }
            Employee employee = db.Employees.Find(id);
            //If Employee not found
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        // POST: Employees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                Employee employee = db.Employees.Find(id);
                db.Employees.Remove(employee);
                db.SaveChanges();
            } catch (DataException /*e*/)
            {
                //Log message from DataException with e.Message
                return RedirectToAction("Delete", new { id = id, saveChanges = true });
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
